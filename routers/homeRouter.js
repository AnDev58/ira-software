const express = require("express")
const controller = require("../controllers/homeController")

const homeRouter = express.Router();
homeRouter.use("^/contacts$", controller.contacts)
homeRouter.use("^/$", controller.index)
module.exports = homeRouter