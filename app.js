const express = require("express");
const hbs = require("hbs");
const expressHbs = require("express-handlebars");
const homeRouter = require("./routers/homeRouter");
const app = express();

app.engine("hbs", expressHbs.engine(
    {
        layoutsDir: "views/layouts", 
        defaultLayout: "layout",
        extname: "hbs"
    }
))
app.set("view engine", "hbs");
hbs.registerPartials(__dirname + "/views/partials");
app.use("/", homeRouter)

app.use(function (req, res, next) {
    res.status(404).send("Not Found")
});

app.listen(3000)