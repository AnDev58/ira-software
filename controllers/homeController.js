exports.index = function (request, response) {
    response.render("index", {title: "Главная"})
}

exports.contacts = function (request, response) {
    response.render("contacts", {
        title: "Мои контакты",
        email: "andre-migin@yandex.ru",
        phone: "+79631119637"
    });
}